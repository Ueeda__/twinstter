package br.com.ghlabs.controllers;

import java.sql.Date;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.ghlabs.models.Tweet;
import br.com.ghlabs.models.Usuario;
import br.com.ghlabs.services.TweetService;
import br.com.ghlabs.services.UsuarioService;

@Controller
public class ControllerTimeLine {
	
	@Autowired
	private UsuarioService serviceUsuario;
	
	@Autowired
	private TweetService serviceTweet;
	
	@RequestMapping(value="/time-line", method=RequestMethod.GET)
	public String timeLine(Model model, Authentication auth, List<Tweet> listaTweets){
		Usuario usuario = (Usuario) auth.getPrincipal();
		model.addAttribute("usuario", usuario);
		model.addAttribute("tweet", new Tweet());
		List<Tweet> todos = serviceTweet.obterTodos(usuario);
		model.addAttribute("followers", serviceUsuario.followers(usuario));
		model.addAttribute("following", serviceUsuario.following(usuario));
		model.addAttribute("tweets", serviceUsuario.tweets(usuario));
		
		if(todos != null){
			model.addAttribute("listaTweet", todos);
			model.addAttribute("quantidadeTweets", todos.size());
		}	
		else{
			model.addAttribute("quantidadeTweets", 0);
		}
		
		
		return "/timeLine/time-line";
	}
	
	@Transactional
	@RequestMapping(value="/time-line", method=RequestMethod.POST)
	public String postarTweet(@Valid Tweet tweet, BindingResult result, Model model, Authentication auth){
		if(result.hasFieldErrors()){
			model.addAttribute("tweet", tweet);
			return "/error";
		}
		Date now = new Date(System.currentTimeMillis());
		tweet.setPublicacaoTweet(now);
		tweet.setDonoTweet((Usuario)auth.getPrincipal());
		serviceTweet.cadastrarTweet(tweet);
		return "redirect:/time-line";
	}
	
	
	
}
