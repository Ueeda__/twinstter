package br.com.ghlabs.controllers;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.ghlabs.models.Usuario;
import br.com.ghlabs.modelviews.EditUsuarioForm;
import br.com.ghlabs.services.UsuarioService;

@Controller
public class ControllerUsuario {
	
	@Autowired
	private UsuarioService service;
	
	@RequestMapping(value="/", method= RequestMethod.GET)
	public String novoUsuario(Model model, Authentication auth){
		if (auth != null)
			return "redirect:/time-line";
		model.addAttribute("usuario", new Usuario());
		return "cadastro-usuario";
	}
	
	@Transactional
	@RequestMapping(value="/", method=RequestMethod.POST )
	public String novo(@Valid Usuario usuario, BindingResult result, Model model) {		
		if(result.hasFieldErrors()) {			
			model.addAttribute("usuario", usuario);
			return "cadastro-usuario";			
		}	
		service.cadastrarUsuario(usuario);
		System.out.println(usuario);
		return "redirect:/login";
	}
	
	@RequestMapping(value="/editar", method= RequestMethod.GET)
	public String editarInformacoesUsuario(Model model, Authentication auth){
		EditUsuarioForm temp = new EditUsuarioForm((Usuario)auth.getPrincipal());
		model.addAttribute("usuario", (Usuario)auth.getPrincipal());
		model.addAttribute("editFormUsuario", temp);
		return "editar-usuario";
	}
	
	@Transactional
	@RequestMapping(value="/editar", method= RequestMethod.POST)
	public String confirmarEdicoesUsuario(@Valid EditUsuarioForm editFormUsuario, BindingResult result, Model model, Authentication auth){
		if(result.hasFieldErrors()){
			model.addAttribute("usuario", (Usuario)auth.getPrincipal());
			model.addAttribute("editFormUsuario", editFormUsuario);
			return "editar-usuario";
		}
		boolean resultadoEditar = service.atualizarInformacao((Usuario)auth.getPrincipal(), editFormUsuario);
		if(!resultadoEditar)
			return "redirect:/editar?status=erro";
		model.addAttribute("usuario", (Usuario)auth.getPrincipal());
		model.addAttribute("editFormUsuario", new EditUsuarioForm((Usuario)auth.getPrincipal()));
		return "redirect:/logout";
	}

	@RequestMapping(value="/excluir", method=RequestMethod.GET) 
	public String excluir(Model model) {		
		return "excluir-usuario";		
	}
	
	@Transactional
	@RequestMapping(value="/excluir", method=RequestMethod.POST) 
	public String excluir(Authentication auth) {		
		Usuario temp;
		temp = (Usuario)auth.getPrincipal();
		service.excluirUsuario(temp);
		return "redirect:/logout";
	}
}
