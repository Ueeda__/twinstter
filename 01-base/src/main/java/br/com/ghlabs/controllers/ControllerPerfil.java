package br.com.ghlabs.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.ghlabs.models.Tweet;
import br.com.ghlabs.models.Usuario;
import br.com.ghlabs.services.TweetService;
import br.com.ghlabs.services.UsuarioService;

@Controller
public class ControllerPerfil {
	
	@Autowired
	private UsuarioService serviceUsuario;
	
	@Autowired
	private TweetService serviceTweet;
	
	@RequestMapping(value="/perfil/{idUsuario}", method= RequestMethod.GET)
	public String retornarPerfil(@PathVariable int idUsuario, Model model, Authentication auth){
		Usuario u = serviceUsuario.buscarUsuario(idUsuario);
		model.addAttribute("perfil", u);
		model.addAttribute("followers", serviceUsuario.followers(u));
		model.addAttribute("following", serviceUsuario.following(u));
		model.addAttribute("tweets", serviceUsuario.tweets(u));
		List<Tweet> todos = serviceTweet.obterTodos(u);
		if(todos != null){
			model.addAttribute("listaTweet", todos);
			model.addAttribute("quantidadeTweets", todos.size());
		}
		else{
			model.addAttribute("quantidadeTweets", 0);
		}
		if(u.getIdUsuario() == idUsuario){
			model.addAttribute("botaoFollow", false);
		}
		else{
			model.addAttribute("botaoFollow", true);
		}
		
		return "/perfil/perfil-user";
	}
}
