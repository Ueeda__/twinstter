package br.com.ghlabs.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.ghlabs.services.UsuarioService;

@Controller
public class ControllerSearch {
	
	@Autowired
	private UsuarioService service;
		
	@RequestMapping(value="/todosUsuarios", method=RequestMethod.GET)
	public String pesquisarUsuarios(Model model){
		model.addAttribute("listaUsuarios", service.buscarTodos());
		return "lista-usuarios";
	}
	
	@RequestMapping(value="/todosUsuarios", method=RequestMethod.POST)
	public String mostrarUsuarios(String userName, Model model){
		if(userName.isEmpty()){
			model.addAttribute("listaUsuarios", service.buscarTodos());
			return "lista-usuarios";
		}
		model.addAttribute("listaUsuarios", service.buscarUsuario(userName));
		return "lista-usuarios";
	}
}
