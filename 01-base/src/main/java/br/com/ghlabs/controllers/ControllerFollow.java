package br.com.ghlabs.controllers;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.ghlabs.models.Usuario;
import br.com.ghlabs.services.UsuarioService;

@Controller
public class ControllerFollow {
	
	@Autowired
	private UsuarioService serviceUsuario;
	
	@Transactional
	@RequestMapping("/follow/{idUsuario}")
	public String seguir(@PathVariable int idUsuario, Authentication auth){
		Usuario usuarioLogado = (Usuario)auth.getPrincipal();
		Usuario usuarioFollow = serviceUsuario.buscarUsuario(idUsuario);
		usuarioLogado.getFollowing().add(usuarioFollow);
		usuarioFollow.getFollowers().add(usuarioLogado);
		serviceUsuario.atualizarUsuario(usuarioLogado);
		serviceUsuario.atualizarUsuario(usuarioFollow);
		return "redirect:/perfil/" + idUsuario;
	}
	
}
