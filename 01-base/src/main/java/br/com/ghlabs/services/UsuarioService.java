package br.com.ghlabs.services;

import java.util.List;

import br.com.ghlabs.models.Usuario;
import br.com.ghlabs.modelviews.EditUsuarioForm;

public interface UsuarioService {
	
	void cadastrarUsuario(Usuario usuario);
	void excluirUsuario(Usuario usuario);
	void atualizarUsuario(Usuario usuario);
	List<Usuario> buscarTodos();	
	int followers(Usuario usuario);
	int following(Usuario usuario);
	int tweets(Usuario usuario);
	List<Usuario> buscarUsuario(String username);	
	Usuario buscarUsuario(int idUsuario);
	boolean atualizarInformacao(Usuario usuarioAtual, EditUsuarioForm novasInformacoes);
}	
