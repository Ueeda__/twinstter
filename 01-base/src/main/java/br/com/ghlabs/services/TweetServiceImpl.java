package br.com.ghlabs.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import br.com.ghlabs.models.Tweet;
import br.com.ghlabs.models.Usuario;
import br.com.ghlabs.repositories.TweetRepository;

@Service
@Primary
public class TweetServiceImpl implements TweetService {

	@Autowired
	private TweetRepository repo;

	@Override
	public void cadastrarTweet(Tweet tweet) {
		repo.inserir(tweet);
	}

	@Override
	public List<Tweet> obterTodos(Usuario usuario) {
		List<Tweet> todosTweets = repo.obterTodos(usuario);

		if (todosTweets.size() > 0)
			return todosTweets;
		
		return null;
	}

}
