package br.com.ghlabs.services;

import java.util.List;

import br.com.ghlabs.models.Tweet;
import br.com.ghlabs.models.Usuario;

public interface TweetService {
	void cadastrarTweet(Tweet tweet);
	
	List<Tweet> obterTodos(Usuario usuario);
}
