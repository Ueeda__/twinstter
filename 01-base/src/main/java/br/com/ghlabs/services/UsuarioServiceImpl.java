package br.com.ghlabs.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.ghlabs.models.Usuario;
import br.com.ghlabs.modelviews.EditUsuarioForm;
import br.com.ghlabs.repositories.UsuarioRepository;

@Service
@Primary
public class UsuarioServiceImpl implements UserDetailsService, UsuarioService{
	
	@Autowired
	private UsuarioRepository repo;
	
	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		Usuario user;
		
		user = repo.buscarUsuarioPorLogin(login);
		
		if( user == null)
			throw new UsernameNotFoundException("Usuário não encontrado.");
	    
		return user;
	}
	
	@Override
	@Transactional
	public void cadastrarUsuario(Usuario usuario) {
		
		BCryptPasswordEncoder encoder;
		encoder = new BCryptPasswordEncoder();
		
		String passwordCriptografado;
		passwordCriptografado = encoder.encode(usuario.getPassword());
		usuario.setPassword(passwordCriptografado);
		
		repo.inserir(usuario);
	}
	
	public void excluirUsuario(Usuario usuario) {		
			repo.excluir(usuario);		
	}	
	
	public List<Usuario> buscarTodos(){
		List<Usuario> todosUsuarios = repo.obterTodos();
		
		if(todosUsuarios.size() > 0)
			return todosUsuarios;
		
		return null;
	}
	
	public List<Usuario> buscarUsuario(String userName){
		List<Usuario> todosUsuarios = repo.buscarUsuario(userName);
		
		if(todosUsuarios.size() > 0)
			return todosUsuarios;
		
		return null;
	}
	
	public int followers(Usuario usuario){
		Usuario u = repo.buscarUsuario(usuario.getIdUsuario());
		int qtd = u.getFollowers().size();
		return qtd;
	}
	public int following(Usuario usuario){
		Usuario u = repo.buscarUsuario(usuario.getIdUsuario());
		int qtd = u.getFollowing().size();
		return qtd;
	}
	
	public int tweets(Usuario usuario){
		Usuario u = repo.buscarUsuario(usuario.getIdUsuario());
		int qtd = u.getTweetsPublicados().size();
		return qtd;
	}
	@Override
	public boolean atualizarInformacao(Usuario usuarioAtual, EditUsuarioForm novasInformacoes) {
		boolean resultadoOperacao = repo.updateInfo(usuarioAtual, novasInformacoes);
		return resultadoOperacao;
	}
	
	public Usuario buscarUsuario(int idUsuario){
		return repo.buscarUsuario(idUsuario);
	}
	
	public void atualizarUsuario(Usuario usuario){
		repo.atualizar(usuario);
	}	
	
}
