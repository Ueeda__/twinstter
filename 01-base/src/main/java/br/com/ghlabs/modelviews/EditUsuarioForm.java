package br.com.ghlabs.modelviews;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.ghlabs.models.Usuario;

public class EditUsuarioForm {	
	
	@NotNull(message="O campo nome não pode ficar vazio")
	@Size(min=5, message="O novo username precisa ter no mínimo 5 caracteres")
	private String novoUsername;
	
	@Size(min=1, message="O novo nome não pode ficar vazio")
	private String novoNome;
	
	public EditUsuarioForm(){}
	
	public EditUsuarioForm(String novoUsername, String novoNome){
		this.novoNome = novoNome;
		this.novoUsername = novoUsername;
	}
	
	public EditUsuarioForm(Usuario u){
		this.novoNome = u.getNomeUsuario();
		this.novoUsername = u.getUsername();
	}

	public String getNovoUsername() {
		return novoUsername;
	}

	public void setNovoUsername(String novoUsername) {
		this.novoUsername = novoUsername;
	}

	public String getNovoNome() {
		return novoNome;
	}

	public void setNovoNome(String novoNome) {
		this.novoNome = novoNome;
	}
}
