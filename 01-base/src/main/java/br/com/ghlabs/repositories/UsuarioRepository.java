package br.com.ghlabs.repositories;

import java.util.List;

import br.com.ghlabs.models.Usuario;
import br.com.ghlabs.modelviews.EditUsuarioForm;

public interface UsuarioRepository {
	
	List<Usuario> obterTodos();
	List<Usuario> buscarUsuario(String userName);
	Usuario buscarUsuario(int idUsuario);
	void inserir(Usuario usuario);	
	void excluir(Usuario usuario);	
	Usuario buscarUsuarioPorLogin(String userName);	
	boolean usuarioExiste(Usuario usuario);	
//	int quantidadeFollowers(Usuario usuario);
	boolean updateInfo(Usuario usuarioAtual, EditUsuarioForm novasInformacoes);
	void atualizar(Usuario usuario);
}
