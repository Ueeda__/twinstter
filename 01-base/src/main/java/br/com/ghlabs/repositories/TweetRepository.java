package br.com.ghlabs.repositories;

import java.util.List;

import br.com.ghlabs.models.Tweet;
import br.com.ghlabs.models.Usuario;

public interface TweetRepository {
	void inserir(Tweet tweet);
	List<Tweet> obterTodos(Usuario usuario);
	
	
}
