package br.com.ghlabs.repositories;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import br.com.ghlabs.models.Tweet;
import br.com.ghlabs.models.Usuario;

@Primary
@Repository
public class JpaTweetRepositorio implements TweetRepository{
	
	@PersistenceContext(name="twinstterPU")
	private EntityManager entityManager;

	@Override
	public void inserir(Tweet tweet) {
		entityManager.persist(tweet);		
	}
	
	public List<Tweet> obterTodos(Usuario usuario) {				
		List<Tweet> listaTweet = new ArrayList<Tweet>();
		String jql = "SELECT t FROM Tweet t WHERE t.donoTweet = (SELECT idUsuario from Usuario WHERE username LIKE :userName)"
				+ "ORDER BY t.publicacaoTweet DESC";
		try{
			listaTweet = entityManager.createQuery(jql, Tweet.class)
					.setParameter("userName", usuario.getUsername())
					.getResultList();
		} catch(NoResultException e){
			return null;
			
		}		
		
		return listaTweet;	
	}
	
	
	
}
