package br.com.ghlabs.repositories;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import br.com.ghlabs.models.Usuario;
import br.com.ghlabs.modelviews.EditUsuarioForm;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Primary
@Repository
public class JpaUsuarioRepository implements UsuarioRepository {

	@PersistenceContext(name="twinstterPU")
	private EntityManager entityManager;
	
	public List<Usuario> obterTodos() {				
		List<Usuario> listaUsuario = new ArrayList<Usuario>();
		String jql = "SELECT u FROM Usuario u";
		Query q;
		try{
			q = entityManager.createQuery(jql, Usuario.class);
			listaUsuario = q.getResultList();
		}
		catch(NoResultException e){
			return null;
		}
		return listaUsuario;
		
	}
	
	public List<Usuario> buscarUsuario(String userName){
		List<Usuario> listaUsuario = new ArrayList<Usuario>();
		String jql = "Select u from Usuario u WHERE u.username LIKE :userName";
		Query q;
		try{
			q = entityManager.createQuery(jql, Usuario.class)
						     .setParameter("userName", userName);
			listaUsuario = q.getResultList();
		}
		catch(NoResultException e){
			return null;
		}
		return listaUsuario;
	}
	
	//Retorna Usuários com o mesmo nome na busca
	public Usuario buscarUsuarioPorLogin(String userName){
		String jql = "Select u from Usuario u WHERE u.login LIKE :userName";
		Usuario usuario;
		try{
			usuario = entityManager.createQuery(jql, Usuario.class)
					.setParameter("userName", userName)
					.getSingleResult();
		} catch(NoResultException e){
			return null;
		}
		return usuario;
	}
	
	// Verifica se o usuário existe
	public boolean usuarioExiste(Usuario usuario){
		String jql = "Select u from Usuario u WHERE u.nomeUsuario LIKE :userName";
		Usuario temp = new Usuario();
		try{
			temp = entityManager.createQuery(jql, Usuario.class)
					.setParameter("userName", usuario.getUsername())
					.getSingleResult();
			if(usuario.getUsername() == temp.getUsername())
				return true;
		}
		catch(NoResultException e){
			
		}
		return false;
	}
	
	//Quantidade de followers
//	public int quantidadeFollowers(Usuario usuario){	
//		int followers =0 ;
//		String jql = "select COUNT(u) from usuario_has_followers u where idUsuario=:idUsuarioAtual ";
//		try{
//			followers = (int)entityManager.createQuery(jql)
//					.setParameter("idUsuarioAtual", idUsuarioAtual)
//					.getSingleResult();
//		}
//		catch(Exception e){
//			e.printStackTrace();
//		}
//		
//		
//		
//		return followers;
//	}
	
	//Comando para inserir
	public void inserir(Usuario usuario) {		
		entityManager.persist(usuario);		
	}
	
	//Comando para atualizar
		public void atualizar(Usuario usuario) {		
			Object managed = entityManager.merge(usuario);
			entityManager.persist(managed);		
		}
	
	//Comando para excluir
	public void excluir(Usuario usuario) {
		Object managed = entityManager.merge(usuario);
		entityManager.remove(managed);
	}

	@Override
	public boolean updateInfo(Usuario usuarioAtual, EditUsuarioForm novasInformacoes) {
		String jql = "UPDATE Usuario SET nomeUsuario = :novoNome, username = :novoUsername"
				+ " WHERE idUsuario = :idUsuarioAtual";
		int updateCount = 0;
		Query q;
		try{
			q = entityManager.createQuery(jql);
			updateCount = q.setParameter("novoNome", novasInformacoes.getNovoNome())
								.setParameter("novoUsername", novasInformacoes.getNovoUsername())
								.setParameter("idUsuarioAtual", usuarioAtual.getIdUsuario())
								.executeUpdate();
		} catch(Exception e){
			e.printStackTrace();
			return false;
		}
		if(updateCount > 0)
			return true;
		return false;
	}
	
	public Usuario buscarUsuario(int idUsuario){
		String jql = "Select u from Usuario u WHERE u.idUsuario LIKE :idUsuario";
		Usuario usuario;
		try{
			usuario = entityManager.createQuery(jql, Usuario.class)
					.setParameter("idUsuario", idUsuario)
					.getSingleResult();
		} catch(NoResultException e){
			return null;
		}
		return usuario;
	}

}
