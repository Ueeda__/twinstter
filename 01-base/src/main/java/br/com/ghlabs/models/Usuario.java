package br.com.ghlabs.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
public class Usuario implements UserDetails{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1463232553495769557L;
	
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idUsuario;
	
	@Column(unique=true)
	@NotNull(message="O campo e-mail não pode ficar vazio")
	
	@Pattern(
			regexp="(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])",
			message="O campo e-mail deve conter um endereço válido"
			)
	@Size(min=1, message="O nome não pode ficar vazio")
	private String login;
	
	@NotNull
	@Size(min=1, message="O nome não pode ficar vazio")
	@Column
	private String nomeUsuario;
	
	@NotNull(message="O campo username não pode ficar vazio")
	@Size(min=5, message="O Username precisa ter no mínimo 5 caracteres")
	@Column(unique=true)
	private String username;
	
//	@DateTimeFormat(pattern = "yyyy-MM-dd")
//	@NotNull(message="A data de nascimento precisa ser fornecida")
//	@Column
//	private Date nascimento;
	
	@Column
	@NotNull(message="O campo senha não pode ficar vazio")
	@NotEmpty(message="O campo senha não pode ficar vazio")
	@Size(min=6, message="O campo senha deve conter mais que 6 caracteres")
	private String password;
	
	@OneToMany(mappedBy="donoTweet")
	private List<Tweet> tweetsPublicados;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name="usuario_has_retweet", joinColumns=
  	{@JoinColumn(name="idUsuario")}, inverseJoinColumns=
    {@JoinColumn(name="idTweet")})
	private List<Tweet> tweetsRetweetados;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name="usuario_has_favorite", joinColumns=
  	{@JoinColumn(name="idUsuario")}, inverseJoinColumns=
    {@JoinColumn(name="idTweet")})
	private List<Tweet> tweetsFavoritos;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name="usuario_has_followers", joinColumns=
	{@JoinColumn(name="idUsuario")}, inverseJoinColumns=
	{@JoinColumn(name="id_usuario_follower")})
	private List<Usuario> followers;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name="usuario_has_following", joinColumns=
	{@JoinColumn(name="idUsuario")}, inverseJoinColumns=
	{@JoinColumn(name="id_usuario_following")})
	private List<Usuario> following;
	
	@ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(name = "user_role", joinColumns = 
    @JoinColumn(name = "idUsuario", referencedColumnName = "idUsuario"), inverseJoinColumns = 
    @JoinColumn(name = "perfil", referencedColumnName = "nome"))
	private Set<Perfil> papeis;
	
	public Usuario(){
		super();
		this.tweetsPublicados = new ArrayList<Tweet>();
		this.tweetsFavoritos = new ArrayList<Tweet>();
		this.tweetsRetweetados = new ArrayList<Tweet>();
		this.followers = new ArrayList<Usuario>();
		this.following = new ArrayList<Usuario>();
		this.papeis = new HashSet<Perfil>();
	}
	
	public Usuario(int idUsuario, String login, String password,
			List<Tweet> tweetsPublicados, List<Tweet> tweetsRetweetados, 
			List<Tweet> tweetsFavoritos, List<Tweet> tweetsFavorios, 
			List<Usuario> followers, List<Usuario> following,
			Set<Perfil> papeis){
		super();
		this.idUsuario = idUsuario;
		this.login = login;
		this.password = password;
		this.tweetsPublicados = tweetsPublicados;
		this.tweetsFavoritos = tweetsFavorios;
		this.tweetsRetweetados = tweetsRetweetados;
		this.followers = followers;
		this.following = following;
		this.papeis = papeis;
	}
	
	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public String getPassword() {
		return password;
	}
	
//	public Date getNascimento() {
//		return nascimento;
//	}
//
//	public void setNascimento(Date nascimento) {
//		this.nascimento = nascimento;
//	}
	
	public List<Tweet> getTweetsPublicados() {
		return tweetsPublicados;
	}

	public void setTweetsPublicados(List<Tweet> tweetsPublicados) {
		this.tweetsPublicados = tweetsPublicados;
	}

	public List<Tweet> getTweetsRetweetados() {
		return tweetsRetweetados;
	}

	public void setTweetsRetweetados(List<Tweet> tweetsRetweetados) {
		this.tweetsRetweetados = tweetsRetweetados;
	}

	public List<Tweet> getTweetsFavoritos() {
		return tweetsFavoritos;
	}

	public void setTweetsFavoritos(List<Tweet> tweetsFavoritos) {
		this.tweetsFavoritos = tweetsFavoritos;
	}

	public List<Usuario> getFollowers() {
		return followers;
	}

	public void setFollowers(List<Usuario> followers) {
		this.followers = followers;
	}

	public List<Usuario> getFollowing() {
		return following;
	}

	public void setFollowing(List<Usuario> following) {
		this.following = following;
	}

	public Set<Perfil> getPapeis() {
		return papeis;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return papeis;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}	
	
	@Override
	public String toString() {
		return "User [idUser=" + idUsuario + ", login=" + login + ", password=" + password
				+ "]";
	}
	
	
	
}
