package br.com.ghlabs.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Tweet {

	@Id 
	@GeneratedValue
	private int idTweet;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date publicacaoTweet;
	
	@Size(max=140)
	@NotNull(message="O campo tweet não pode ficar vazio")
	private String conteudoTweet;
	
	@ManyToOne
	@JoinColumn(name="id_usuario")
	private Usuario donoTweet;
	
	public Tweet(){
		
	}
	
	public Tweet(Usuario donoTweet, Date publicacaoTweet, String conteudoTweet){
		this.donoTweet = donoTweet;
		this.publicacaoTweet = publicacaoTweet;
		this.conteudoTweet = conteudoTweet;
	}

	public int getIdTweet() {
		return idTweet;
	}

	public void setIdTweet(int idTweet) {
		this.idTweet = idTweet;
	}

	public Date getPublicacaoTweet() {
		return publicacaoTweet;
	}

	public void setPublicacaoTweet(Date publicacaoTweet) {
		this.publicacaoTweet = publicacaoTweet;
	}

	public String getConteudoTweet() {
		return conteudoTweet;
	}

	public void setConteudoTweet(String conteudoTweet) {
		this.conteudoTweet = conteudoTweet;
	}

	public Usuario getDonoTweet() {
		return donoTweet;
	}

	public void setDonoTweet(Usuario donoTweet) {
		this.donoTweet = donoTweet;
	}
	
}
