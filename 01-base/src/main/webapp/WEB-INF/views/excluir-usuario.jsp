<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page session="true"%>

<jsp:include page="/WEB-INF/views/includes/logado/header-logado.jsp">
    <jsp:param name="pageTitle" value="Twinstter - Excluir"/>
</jsp:include>
<div class="container">
	<div class="col-sm-6 col-sm-offset-3">
		<form:form>		
			<h3 style="text-align: center;">Voc� tem certeza que quer excluir a conta?</h3>
			<input type="submit" class="btn-4" value="Sim" />  
		</form:form>
	</div>
</div>	

<jsp:include page="/WEB-INF/views/includes/footer.jsp" />