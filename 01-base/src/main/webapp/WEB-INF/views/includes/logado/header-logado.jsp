<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authentication property="principal" var="currentUser"/>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
  	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<spring:url value="/resources/style.css" var="css" />
	<spring:url value="/resources/functions.js" var="js" />
	<link rel="stylesheet" type="text/css" href="${css}">	
	<title>${ param.pageTitle }</title>
</head>
<body>

<div class="col-md-12 header-index">
	<div class="container">
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
			  <li><a class="navbar-brand" href="/01-base/"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
			  <li><a href="/01-base/todosUsuarios">Buscar Usu�rios</a></li>
			</ul>
			<div class="dropdown nav navbar-nav navbar-right">
	              <button class="btn btn-menu dropdown-toggle" type="button" data-toggle="dropdown">Menu
	              <span class="caret"></span></button>
	              <ul class="dropdown-menu">
	                  <li><a href="/01-base/editar">Editar informa��es</a></li>
	                  <li><a href="/01-base/excluir">Excluir conta</a></li>
	                  <li><a href="/01-base/perfil/${ currentUser.idUsuario }">Perfil</a></li>
	                  <li><a href="/01-base/logout">Sair</a></li>
	              </ul>
	        </div>
		</div>
	</div>
</div>