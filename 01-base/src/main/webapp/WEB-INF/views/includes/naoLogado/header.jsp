<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html lang="pt-br">
<head>
	<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
  	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<spring:url value="/resources/style.css" var="css" />
	<spring:url value="/resources/functions.js" var="js" />
	<link rel="stylesheet" type="text/css" href="${css}">	
	<title>${ param.pageTitle }</title>
</head>
<body>
<div class="col-md-12 header-index">
  <div class="container">
	<ul class="nav navbar-nav">
		<li><a href="/01-base/"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
	</ul>
	<ul class="nav navbar-nav navbar-right">
	  <li class="btn-entrar"><a href="/01-base/login">Login</a></li>
	</ul>
  </div>
</div>
