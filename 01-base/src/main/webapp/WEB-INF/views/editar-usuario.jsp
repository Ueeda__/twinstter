<jsp:include page="/WEB-INF/views/includes/logado/header-logado.jsp">
	<jsp:param name="pageTitle" value="Twinstter - Editar informa��es"/>
</jsp:include>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

	<div class="container">
	     <div class="col-md-6 col-md-offset-3" style="height: 90vh;">
	         <div class="col-md-12 centro">
	             <div class="col-md-12">
	                <h2 class="title-index"> Suas informa��es.</h2>
	                <sf:form modelAttribute="editFormUsuario">
	                	<div class="row linhaCadastro">
	                		<div class="col-sm-4 label-cadastro">
		                		<sf:label path="novoUsername"> Username: </sf:label>
		                	</div>
							<div class="col-sm-8">
								<sf:input path="novoUsername"/>
								<sf:errors path="novoUsername"> </sf:errors>
							</div>
	                	</div>
	                	
	                	<div class="row linhaCadastro">
	                		<div class="col-sm-4 label-cadastro">
		                		<sf:label path="novoNome"> Nome: </sf:label>
		                	</div>
							<div class="col-sm-8">
								<sf:input path="novoNome"/>
								<sf:errors path="novoNome"> </sf:errors>
							</div>
	                	</div>
						
		               	<input type="submit" class="btn-inscreva-se" value="Atualizar" />
		               	
		               	<span>Obs: Ao apertar no bot�o Atualizar, voc� precisar� logar novamente</span>
		               	
	                </sf:form>
	               </div>
	         </div>
	     </div>
    </div> 
<jsp:include page="/WEB-INF/views/includes/footer.jsp" />