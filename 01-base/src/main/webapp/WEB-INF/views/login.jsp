<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page session="true"%>

<jsp:include page="/WEB-INF/views/includes/naoLogado/header.jsp">
    <jsp:param name="pageTitle" value="Twinstter - Login"/>
</jsp:include>

<div class="col-md-offset-4 col-md-4">
  <div class="container-fluid">
   <h4 class="text-center title-login">Entrar no Twinstter</h4>
   <hr />
   
   <form:form servletRelativeAction="/login">
   	<div class="form-group">
   		<label for="login">E-mail:</label>
   		<input type="text" name="username" class="form-control input-sm" />
   	</div>
   	
   	<div class="form-group">
   		<label for="password">Senha:</label>
   		<input type="password" name="password" class="form-control input-sm" />
   	</div>
   	
   	<span class="group-btn">   
   		<input type="submit" value="Entrar" class="btn btn-inscreva-se btn-login" />  
   	</span>
   </form:form>
   	
   <br />
   <p class="text-warning text-center">
   	<a href="/01-base/" class="link-padrao">Caso n�o tenha conta, clique aqui para se cadastrar.</a>
   </p>
  </div>
</div>

<jsp:include page="/WEB-INF/views/includes/footer.jsp" />