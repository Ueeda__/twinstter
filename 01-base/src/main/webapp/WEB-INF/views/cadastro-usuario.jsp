<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="/WEB-INF/views/includes/naoLogado/header.jsp">
    <jsp:param name="pageTitle" value="Twinstter - Cadastro"/>
</jsp:include>


<div class="container">
     <div class="col-md-6 col-md-offset-3" style="height: 100vh;">
         <div class="col-md-12 centro">
             <div>
                <h2 class="title-index"> Participe hoje do Twistter.</h2>
                <sf:form modelAttribute="usuario">
                	<div class="row linhaCadastro">
                		<div class="col-sm-4 label-cadastro">
	                		<sf:label path="login"> E-mail: </sf:label>
	                	</div>
						<div class="col-sm-8">
							<sf:input path="login"/>
							<sf:errors path="login"> </sf:errors>
						</div>
                	</div>
                	
                	<div class="row linhaCadastro">
                		<div class="col-sm-4 label-cadastro">
	                		<sf:label path="username"> Username: </sf:label>
	                	</div>
						<div class="col-sm-8">
							<sf:input path="username"/>
							<sf:errors path="username"> </sf:errors>
						</div>
                	</div>
                	
                	<div class="row linhaCadastro">
                		<div class="col-sm-4 label-cadastro">
	                		<sf:label path="nomeUsuario"> Nome: </sf:label>
	                	</div>
						<div class="col-sm-8">
							<sf:input path="nomeUsuario"/>
							<sf:errors path="nomeUsuario"> </sf:errors>
						</div>
                	</div>
                	
					<div class="row linhaCadastro">
                		<div class="col-sm-4 label-cadastro">
	                		<sf:label path="password"> Senha: </sf:label>
	                	</div>
						<div class="col-sm-8">
							<sf:input path="password" type="password"/>
							<sf:errors path="password"> </sf:errors>
						</div>
                	</div>			
					
	               	<input type="submit" class="btn-inscreva-se" value="Criar" />
                </sf:form>
                 <div class="col-md-12">
                   <p class="texto-index">Ao inscrever-se, voc� concorda com os Termos de Servi�o e a Pol�tica de Privacidade, incluindo o Uso de Cookies. Outros poder�o encontr�-lo por e-mail ou n�mero de celular, quando fornecido.</p>
                 </div>
             </div>
         </div>
     </div>
 </div>
 
<jsp:include page="/WEB-INF/views/includes/footer.jsp" />
