<jsp:include page="/WEB-INF/views/includes/logado/header-logado.jsp">
	<jsp:param name="pageTitle" value="Twinstter - Editar informa��es"/>
</jsp:include>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>	

<!-- FEED -->
<div class="col-md-12 feed">
    <div class="container">
        <div class="col-md-3">
            <div class="resumo-perfil col-md-12">
                <div class="row informacoes-rapidas-perfil">
                    <div class="col-md-5 grid-imagem-perfil">
                        <img src="assets/images/imagem-perfil.jpg" class="imagem-perfil">
                    </div>
                    <div class="col-md-7 grid-usuario-perfil">
                        <a href="">${ perfil.nomeUsuario }<br>@${ perfil.username }</a>
                    </div>
                </div>
                <div class="row resumo-conta">
                    <div class="col-md-4">
                        <a href="">FOLLOWERS<br>${ followers }</a>
                    </div>
                    <div class="col-md-4">
                        <a href="">FOLLOWING<br>${ following }</a>
                    </div>
                    <div class="col-md-4">
                        <a href="">TWEETS<br>${ tweets }</a>
                    </div>
                </div>
	            <c:if test="${ botaoFollow == true }">
	            	<div class="row">
		            	<div class="col-sm-12">
		            		<a href="/01-base/follow/${perfil.idUsuario}" class="btn-4">Seguir</a>
		            	</div>
		            </div>
	            </c:if>
            </div>
        </div>

        <div class="col-md-6 feed-tweets">
            <c:if test="${ quantidadeTweets > 0 }">
	            <c:forEach items="${ listaTweet }" var="tweet">
		        	<div class="row content-tweet">
		                <div class="col-md-12">
		                    <div class="col-md-2">
		                        <img src="assets/images/imagem-perfil.jpg" class="imagem-perfil">
		                    </div>
		                    <div class="col-md-9">
		                        <a href="">${ tweet.donoTweet.nomeUsuario }</a><small>&nbsp;&nbsp;&nbsp;@${ tweet.donoTweet.username }</small>
		                        <p>${ tweet.conteudoTweet }</p>
		                        <div class="row">
		                            <div class="col-md-6">
		                                <div class="col-md-3">
		                                    <i class="fa fa-reply" aria-hidden="true"></i>
		                                </div>
		                                <div class="col-md-3">
		                                    <i class="fa fa-refresh" aria-hidden="true"></i>
		                                </div>
		                                <div class="col-md-3">
		                                    <i class="fa fa-heart" aria-hidden="true"></i>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </div>
		       	</c:forEach>
            </c:if>
            <c:if test="${ quantidadeTweets == 0 }">
            	<h2>O usu�rio ainda n�o Tweetou nada!</h2>
            </c:if>
        </div>

        <div class="col-md-3 links-uteis">
            <div class="col-md-12 listagem-links">
                <a href="">Sobre</a>
                <a href="">@ 2016 TwinTer</a>
            </div>
            <div class="col-md-12 rodape-grids">
                <a href="">Pol�ticas de Privacidade</a>
            </div>
        </div>


    </div>
</div>
<!-- /FEED -->

<jsp:include page="/WEB-INF/views/includes/footer.jsp" />