<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="/WEB-INF/views/includes/logado/header-logado.jsp">
    <jsp:param name="pageTitle" value="Twinstter - Lista de Usu�rios"/>
</jsp:include>


<div class="container">
	<div class="insere-usuario row"> 
		<div class="form-group col-sm-6 col-sm-offset-3">
			<sf:form attribute="userName" method="POST">
		   		<label>Procurar usuario:</label>
		   		<input type="text" name="userName" class="form-control input-sm"/>
				<input type="submit" class="btn-4" value="Procurar" />
			</sf:form>
		</div>
	</div>
	
	<div class=" listagem-usuarios">
		<c:forEach items="${listaUsuarios}"  var="u">
			<div class="col-sm-4 usuario-bloco-listagem">
				<div class="col-sm-4 imagem-usuario-listagem">
					<img src="/01-base/resources/images/imagem-perfil.jpg">
				</div>
				<div class="col-sm-8">
					<div class="col-sm-12">
						<h3 class="username-listagem">@${u.username}</h3>
					</div>
					<div class="col-sm-12">
						<p class="nome-usuario-listagem"> ${u.nomeUsuario}</p>
					</div>
					<a href="/01-base/perfil/${u.idUsuario}" class="btn-3">Ver Perfil</a>
				</div>
			</div>
		</c:forEach>
	</div>

</div>

<jsp:include page="/WEB-INF/views/includes/footer.jsp" />